FROM node:12-alpine

MAINTAINER dangkiena3@gmail.com

WORKDIR /app

ADD package.json package-lock.json /app/
RUN npm install 

ADD . /app

CMD ["npm", "start"]
