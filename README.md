# Demo Gitlab CI AAAAAAAAAAAAAAAAAAAAA

Demo building application with Gitlab CI

## Cấu trúc project

```bash
root
├── app-chart                   # Thư mục chart
│   ├── Chart.yaml
│   ├── templates
│   │   ├── _helpers.tpl
│   │   ├── deployment.yaml     # Định nghĩa deployment
│   │   ├── ingress.yaml        # Định nghĩa ingress
│   │   └── service.yaml        # Định nghĩa service
│   └── values.yaml             # File values mặc định của ứng dụng (không cần sửa)
│
├── index.js
├── package.json
├── package-lock.json
│
├── .gitlab-ci.yml              # File config của Gitlab CI
├── Dockerfile                  # Dockerfile của ứng dụng
└── values-dev.yaml             # File custom value cho môi trường dev
```

File `.gitlab-ci.yml` được cấu hình 2 job là build và deploy:

```yaml
image: docker:18.09.7                   # Image mặc định cho tất cả các bước (trừ khi bị override)

stages:                                 # Định nghĩa các bước
  - build
  - deploy

variables:                              # Định nghĩa 1 số biến môi trường dùng chung mọi bước
  IMAGE_TAG: $CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA
  APP_NAME: node-app

build:                                  # Tên bước, đặt tùy ý
  stage: build                          # Bước build, giống cái stages khai báo ở trên 
  services:
    - docker:18.09.7-dind               # Dùng dind để build docker image trong docker runner
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker build -t ${CI_REGISTRY_IMAGE}:${IMAGE_TAG} .
    - docker push ${CI_REGISTRY_IMAGE}:${IMAGE_TAG}
  only:
    - master                            # Chỉ build code khi branch master thay đổi

deploy_dev:
  stage: deploy
  image: dtzar/helm-kubectl:3.3.4       # Nếu 1 bước dùng 1 image khác image tổng thì set ở đây
  variables:
    NAMESPACE: $DEV_NAMESPACE
  script:
    - export KUBECONFIG=${DEV_KUBECONFIG}   
    - >                                 # Dấu này để biểu thị lệnh sau là 1 line
      helm upgrade --install
      -n ${NAMESPACE}
      -f values-dev.yaml
      --set "image.repository=${CI_REGISTRY_IMAGE}"
      --set "image.tag=${IMAGE_TAG}"
      ${APP_NAME} ./app-chart
  only:
    - master
```

> Lưu ý: DEV_KUBECONFIG là biến môi trường dạng file của Gitlab CI, đang có 1 bug là buộc phải dùng bằng export trực tiếp trong phần script chứ không được dùng trong variables.\

> Docker image dùng trong bài là bản 18.09.7 để tránh lỗi với gitlab CI (bị lỗi với bản 19)

## Thực hành 1: Build ứng dụng với Gitlab CI

- Fork project về repo của mình (để thực hiện CI/CD riêng)
- Mở file `.gitlab-ci.yml` để xem job làm việc gì
- Sửa file readme, hoặc 1 file bất kỳ, sau đó commit và push lên repo.
- Mở menu `CI/CD > Pipelines` xem kết quả job build
- Mở menu `Package & Registries > Container Registry` để xem thông tin image đã được build thành công

## Thực hành 2: Deploy ứng dụng với Gitlab CI

- Chỉnh sửa file `values-dev.yaml` và kiểm tra `deployment.yaml`, `ingress.yaml`, `service.yaml` trong `app-chart/templates` để hiểu những param bên trong `values-dev.yaml`
- Mở menu `Settings > CI/CD > Variables` trong project gitlab của mình
- Thêm 2 biến môi trường là `DEV_NAMESPACE` (là tên namespace của mọi người) và `DEV_KUBECONFIG` dạng file chứa nội dung file KUBECONFIG down từ rancher. Nhớ bỏ tick protect variable.
- Sửa lại file `.gitlab-ci.yml` và bỏ comment phần job deploy và stage deploy.
- Sau khi sửa xong commit và push code lên repo.
- Mở menu `CI/CD > Pipelines` để theo dõi quá trình chạy job
- Trỏ domain đã đăng ký trong `values-dev.yaml` vào file `/etc/hosts` dưới local như bài trước
- Vào thử link `http://<domain-name>:<nginx-port>` xem app đã chạy chưa


